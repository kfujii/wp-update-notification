<?php

require(__DIR__ . '/vendor/autoload.php');

use GuzzleHttp\Client;
use Illuminate\Support\Collection;

$config = require(__DIR__ . '/config.php');

if (!isset($config['path'])) {
    exit();
}

// check-update
$versions = array_map(function($path) {
    $wpCli = __DIR__ . '/vendor/bin/wp';
    $e = function($str) {
        return escapeshellarg($str);
    };

    $command = sprintf('%s core check-update --path=%s --format=json', $wpCli, $e($path));
    ob_start();
    passthru($command, $code);
    $message = json_decode(ob_get_contents());
    ob_end_clean();
    return [
        'path' => $path,
        'versions' => $message,
    ];
}, $config['path']);

// post to slack
array_map(function($v) use ($config) {
    $wpCli = __DIR__ . '/vendor/bin/wp';

    if (!$v['versions']) {
        return;
    }

    // get wp options
    $command = sprintf('%s option list --path=%s --format=json', $wpCli, $v['path']);

    ob_start();
    passthru($command, $code);
    $options = new Collection(json_decode(ob_get_contents(), true));
    ob_end_clean();

    $versions = new Collection($v['versions']);

    $blogName = $options->where('option_name', 'blogname')->first()['option_value'];
    $siteUrl = $options->where('option_name', 'siteurl')->first()['option_value'];

    // create message & post
    $client = new Client();
    $payload = $config['slack']['payload'];
    $payload['attachments'] =  [
        [
            'color' => 'warning',
            'fields' => [
                [
                    'title' => 'サイト名',
                    'value' => sprintf('<%s|%s>', $siteUrl, $blogName),
                    'short' => true
                ],
                [
                    'title' => 'アップデート可能なバージョン',
                    'value' => $versions->implode('version', ', '),
                    'short' => true
                ],
            ]
        ]
    ];

    try {
        $response = $client->request('POST', $config['slack']['webhook_url'], ['form_params' => [
            'payload' => json_encode($payload)
        ]]);
    } catch(\Exception $e) {
        // 流す^^
    }
}, $versions);
