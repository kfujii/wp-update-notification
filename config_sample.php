<?php
/**
 * config.phpの素。
 */
return [
    'path' => [
        '/path/to/your/wordpress/directory',
    ],
    'slack' => [
        'webhook_url' => 'https://hooks.slack.com/services/see/your/integration/setting',
        'payload' => [
            'username' => 'wp-notice',
            'text' => '以下のWordPressサイトはアップデート可能です',
            'cannel' => '#general'
        ]
    ]
];
