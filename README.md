# Webhook経由でSlackにWordpressのバージョンを通知する（雑な）スクリプト

[WP-CLI](https://github.com/wp-cli/wp-cli)の`core check-update`して結果をSlackにポストするスクリプト。

手動、もしくはcronかなんかで走らせることを想定してます。

# インストール

展開したディレクトリで

```
$ composer install
```

# 設定

* config_sample.phpを参考にしてconfig.phpを書きませう。
* `path`はインストール済のWordPressのディレクトリのリスト
* `slack`はslackに投稿するときの設定です。`payload`の中身については[Slackのドキュメント](https://api.slack.com/incoming-webhooks)を参照しませう。そのまま使われます

# 使う

```
$ php wp_update_notification.php
```
